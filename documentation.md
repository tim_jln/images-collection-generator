> Ce petit outil a été farbriqué sur mon temps libre, avec l'aide de ChatGPT, un soir de 30 avril 2024


# Documentation

## Utiliser pour votre collection

1. Installer Node et npm : https://docs.npmjs.com/downloading-and-installing-node-js-and-npm

2. Installer les différents paquets nécessaires :

```
npm run install
```

3. Placer des dossiers/sous-dossiers dans le répertoire "images"

4. Pour consulter une version locale un serveur local :

```
npx @11ty/eleventy --serve
```

5. Lancer un serveur local mais avec davantage d'infos de debug ([doc](https://www.11ty.dev/docs/debugging/))

```
DEBUG=Eleventy* npx @11ty/eleventy --serve
```

6. Générer le site. Le site est créé dans le répertoire `/_site

```
npx @11ty/eleventy
```

⚠️ Pensez à renseigner les paramètres dans `_data/settings.json` (voir ci-dessous)

## Paramétrer et personnaliser

### Paramètres

- ajouter vos images dans un dossier /images/ 
- modifier _data/settings.json avec vos informations
- le paramètre `mergeSubFolders` (`true` ou `false`) permet de choisir entre deux paramètres : dossiers de même noms fusionnés, ou non fusionnés. Explications ci-dessous
- `baseurl` : à configurer avant de générer le site, en inscrivant l'URL où votre site sera hébergé si vous comptez le mettre en ligne. Si Eleventy indique une autre url que http://localhost:8080 pour la version locale, pensez à ajuster le paramètre.

### Classement, tags et dossiers

Pour comprendre la différence voir la structure de tri des images dans le dossier "/images" ci-dessous. Dans mon cas elles sont triées par années :

```
2024-images
  |_ art
  |   |_ painting
  |   |_ sculpture
  |_ cartes
2023-images
  |_ art
  |   |_ dessin
  |   |_ photo
  |_ voyage
    |_ europe
    |_ asie
    |_ ameriques
2022-images
  |_ cartes
    |_ france
    |_ monde
```

Avec `mergeSubFolders` sur `true`, les dossiers de même nom, ici 2024-images/art et 2023-images/art, 2024-images/cartes et 2022-images/cartes seront regroupés et afficheront les images des des dossiers en une seule page.

Avec `mergeSubFolders` sur `false`, la structure de dossier sera préservée, et on aura donc des dossiers imbriqués les uns dans les autres et donc plusieurs dossiers art et cartes différents.

Activer ou non revient à choisir entre conserver un classeemnt hiérarchique ou bien utiliser les dossiers comme des catégories qui seraient appliquées aux images. 

**Tri par tags (keywords)** : pour compléter ce tri, vous pouvez utiliser les métadonnées `Keywords` de vos images (dans les métadonnées IPTC ou XMP ou les deux). Personellement j'utilise [gThumb](https://fr.wikipedia.org/wiki/GThumb) qui est un super gestionnaire d'images et photos. C'est aussi avec ce logiciel que je tri mes photos. En faisant clic droit/commentaire sur une image, vous pouvez ajouter des métadonnées.

### Métadonnées utilisées 

Pour l'instant cet outil n'utilise qu les métadonnées keywords et description.

### Collection dans un sous répertoire

Pour que la collection soit dans un sous répertoire, dans index.njk remplacer

```
permalink: "{% if pagination.pageNumber > 0 %}{{ pagination.pageNumber + 1 }}/{% endif %}index.html"
```

Par ceci par exemple
```
permalink: "{% if pagination.pageNumber > 0 %}gallery/{{ pagination.pageNumber + 1 }}/{% endif %}index.html"
```

## Comprendre

Cet outil utilise Eleventy, [11ty.dev](https://www.11ty.dev/), un système qui permet, à partir de fichiers, de générer un site web.

### Dossiers

- `_data` : contient les paramètres (`settings.json`) et le script pour générer une base de vos fichiers images, `generate_imageCollection_Data.js`. Ce fichier va parcourir tout votre dossier /images et collecter les informations utiles de vos images. Il produit ensuite le fichier le plus important : `imagesCollection.json`. Ce fichier contient la liste de toutes vos images avec leurs métadonnées, et y ajoute quelques infos comme le nom du fichier, l'emplacement dans la structure de dossier, un identifiant unique.
- `_includes` : ici se trouvent les "templates" ou modèles de pages et segments du site. Le fichier `default.html` est la structure de base de vos pages en HTML, et les autres fichiers permettent de coder une seule fois des éléments qui apparaitront dans plusieurs pages. C'est le cas par exemple du module de pagination ou du module de gallerie d'images.
- `_site` : c'est le dossier où Eleventy a généré votre site. **C'est le seul dossier à envoyer en ligne** pour utiliser le site.
- `assets` : pour personnaliser l'apprence de votre site en `styles.css` et son interactivité avec `scripts.js` 
- `node_modules` : c'est le dossier qui contient les paquets (programmes) nécessaires au fonctionnement de tout ceci, y compris Eleventy. Il n'est nécessaire qu'à la génération du site.
- `images` : dossier à créer et dans lequel ajouter vos images

## Notes 

Il était auparavant nécessaire de générer le fichier de base de metadonnées du dossier "images" qui sera placé dans, `_data/imagesCollection.json`  avec la commande `node ./generate_imageCollection_Data.js`. Désormais, le script est dans le dossier _data et considéré comme un [JS Data file](https://www.11ty.dev/docs/data-js/) par Eleventy, donc généré systématiquement.

## Changelog 

- v0.2.0 : prise en compte de la structure des dossiers avec un paramètres pour les utiliser de deux manières différentes
- v0.1.1 : changement de noms pour scripts et fichiers et déplacement du script dans le dossier _data. Mise à jour de la doc.
- v0.1.0 : paginated image gallery, tags and description used from image exif data, tag pages

## Objectif v1.0

- ajouter une page par image (rendre la chose activable/désactivable)
- compléter la documentation du _use case_
- documenter la personnalisation pour les non initiés
- styliser le classement par dossier

## Idées pour le futur (si j'ai le temps)

- support pour les vidéos ?
- ajouter une pagination pour les pages des tags et des dossiers