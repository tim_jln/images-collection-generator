module.exports = function(eleventyConfig) {

  const settings = require("./_data/settings.json");
  eleventyConfig.addPlugin(UrlPlugin, { base: "http://localhost:8080/" });

  // Copy images to output directory
  eleventyConfig.addPassthroughCopy("images");
  eleventyConfig.addPassthroughCopy("assets");

  eleventyConfig.addCollection("images", function(collection) {
    const imagesCollection = require("./_data/imagesCollection.json");

    return imagesCollection.map(image => {
      return {
        id: image.id,
        url: `/${image.id}.html`, // Use the image ID as the URL
        data: image // Pass the image data as front matter for the page
      };
    });
  });

  // Create collection for tags
  eleventyConfig.addCollection("tags", function(collection) {
    const imagesCollection = require("./_data/imagesCollection.json");
    const tagsMap = new Map();

    // Collect tags from imagesCollection
    imagesCollection.forEach(image => {
      if (image.exif && image.exif.Keywords) {
        const keywords = image.exif.Keywords.split(',').map(keyword => keyword.trim());
        keywords.forEach(keyword => {
          if (!tagsMap.has(keyword)) {
            tagsMap.set(keyword, []);
          }
          tagsMap.get(keyword).push(image);
        });
      }
    });
    // console.log(tagsMap);
    // Create tag objects with name, URL, and associated images
    return Array.from(tagsMap).map(([tagName, images]) => {
      return {
        name: tagName,
        url: `/tag/${tagName}.html`, // Use the tag name as part of the URL
        images: images // Pass the associated images for each tag
      };
    });
  });

  // Add an option to control merging of subfolders
  const mergeSubfolders = settings.mergeSubFolders; // Set to false to preserve parentality
  // Create collection for folders and subfolders
  eleventyConfig.addCollection("folders", function(collection) {
    // Load image data JSON file
    const imageData = require('./_data/imagesCollection.json');

    // Map to store folders and their associated images
    const foldersMap = new Map();

    // Populate foldersMap with images
    imageData.forEach(image => {
      const folders = image.path.split('/').filter((folder, index, foldersArray) => {
        // Exclude file name, empty strings, and '/images/' segment
        return index < foldersArray.length - 1 && folder.trim() !== '' && folder !== 'images';
      });

      // Determine folder structure based on mergeSubfolders option
      let folderPath = mergeSubfolders ? folders[folders.length - 1] : folders.join('/');
      const folderUrl = `/folder/${encodeURIComponent(folderPath)}/index.html`;
      const filePath = `/folder/${folderPath}/index.html`;
      const parents = mergeSubfolders ? [] : folders.map((_, index) => {
        return `/folder/${folders.slice(0, index + 1).join('/')}/index.html`;
      });

        if (!foldersMap.has(folderPath)) {
          foldersMap.set(folderPath, {
            name: folders[folders.length - 1],
            url: folderUrl,
            path: filePath,
            parents: parents,
            images: [],
          });
        }
        foldersMap.get(folderPath).images.push(image);
      });

    // Generate collection of folders and subfolders with associated images
    return Array.from(foldersMap.values());
  });


  return {
    dir: {
        input: "./",
        output: "_site",
    }
  }
};

// https://github.com/11ty/eleventy/issues/2387#issuecomment-1127063102
function UrlPlugin(eleventyConfig={}, pluginConfig={}) {
  eleventyConfig.addFilter("absoluteUrl", function (url="", base=pluginConfig.base) {
    try {
      return new URL(url, base).href;
    } catch (err) {
      console.error(err);
      return url;
    }
  });
};